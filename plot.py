import matplotlib.pyplot as plt
import WerteLiveWalk

from walk import WalkSim

walkSim = WalkSim()

walkSim.runCycle()


#plt.plot(walkSim.phaseTrunkValues, label="phaseTrunk", color="black")
#plt.plot(walkSim.phaseLegRValues, label="phaseLegR", color="gray")
#plt.plot(walkSim.rHipRollValues, label="rHipRoll")
#plt.plot(walkSim.phaseShortRValues, label="phaseShortR", color = "purple")
#plt.plot(walkSim.shortRValues, label="shortR")
#plt.plot(walkSim.phaseSwingRValues, label = "phaseSwing", color = "orange")
#plt.plot(walkSim.fSwingRValues, label = "fSwingR")

##############################
"""
plt.plot(walkSim.leftKneePitchValues, label="leftKneePitch")
plt.plot(walkSim.rightKneePitchValues, label="rightKneePitch")
plt.plot(walkSim.leftAnklePitchValues, label="leftAnklePitch")
plt.plot(walkSim.rightAnklePitchValues, label="rightAnklePitch")
plt.plot(walkSim.leftHipPitchValues, label="leftHipPitch")
plt.plot(walkSim.rightHipPitchValues, label="rightHipPitch")
plt.plot(walkSim.leftHipRollValues, label="leftHipRoll")
plt.plot(walkSim.rightHipRollValues, label="rightHipRoll")
plt.plot(walkSim.leftAnkleRollValues, label="leftAnkleRoll")
plt.plot(walkSim.rightAnkleRollValues, label="rightAnkleRoll")
#plt.plot(walkSim.hipYawPitchValues, label="hipYawPitch")
"""

#"""
#NAOQI_VALUES
#right
plt.plot(WalkAngleValues.RHipYawPitchValues, label="RHipYawPitch")
plt.plot(WalkAngleValues.RHipRollValues, label="rHipRoll")
plt.plot(WalkAngleValues.RHipPitchValues, label="rHipPitch")
plt.plot(WalkAngleValues.RKneePitchValues, label="rKneePitch")
plt.plot(WalkAngleValues.RAnklePitchValues, label="rAnklePitch")
plt.plot(WalkAngleValues.RAnkleRollValues, label="rAnkleRoll") 
#left
plt.plot(WalkAngleValues.LHipYawPitchValues, label="lHipYawPitch")
plt.plot(WalkAngleValues.LHipRollValues, label="lHipRoll")
plt.plot(WalkAngleValues.LHipPitchValues, label="lHipPitch")
plt.plot(WalkAngleValues.LKneePitchValues, label="lKneePitch")
plt.plot(WalkAngleValues.LAnklePitchValues, label="lAnklePitch")
plt.plot(WalkAngleValues.LAnkleRollValues, label="lAnkleRoll") 
plt.grid(True)
#"""
plt.legend(bbox_to_anchor=(0.7, 1), loc=0,
           ncol=2, mode="expand", borderaxespad=0.)

plt.show()