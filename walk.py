import math
import config


class WalkSim:

    def __init__(self):
        self.reset()

    def reset(self):
        self.startLength = 40.0
        self.dampStop = self.startLength
        self.balanceGyro1  =  0.0
        self.balanceGyro2  =  0.0
        self.balanceGyro3  =  0.0
        self.balanceGyro4  =  0.0
        self.smoothGyroX   =  0.0
        self.smoothGyroY   =  0.0
        self.walkVecX      =  0.0 # Robo moving at full speed...
        self.walkVecY      =  0.0
        self.walkVecA      =  0.0
        self.bodyRoll      =  0.0
        self.bodyPitch     =  0.0
        self.stabPitch     =  0.0
        self.stabRoll      =  -self.smoothGyroX  *  15.0
        self.rightHipPitch = 0.0 # This is not the default value. What is the default value?
        self.leftHipPitch  = 0.0 # Again, not the default.
        self.cnt = 0.0
        self.it  = 0  #iter

        self.phaseTrunkValues       =  []
        self.phaseLegRValues        =  []
        self.RHipRollValues         =  []
        self.phaseShortRValues      =  []
        self.shortRValues           =  []
        self.phaseSwingRValues      =  []
        self.fSwingRValues          =  []
        self.leftKneePitchValues    =  []
        self.rightKneePitchValues   =  []
        self.leftAnklePitchValues   =  []
        self.rightAnklePitchValues  =  []
        self.leftHipPitchValues     =  []
        self.rightHipPitchValues    =  []
        self.leftHipRollValues      =  []
        self.rightHipRollValues     =  []
        self.leftAnkleRollValues    =  []
        self.rightAnkleRollValues   =  []
        self.hipYawPitchValues      =  []
        self.leftHipPitchValues     =  []
        self.rightHipPitchValues    =  []
        
    def saveValues(self):
        self.phaseTrunkValues.append(self.phaseTrunk)
        self.phaseLegRValues.append(self.phaseLegR)
        self.RHipRollValues.append(self.RHipRoll)
        self.phaseShortRValues.append(self.phaseShortR)
        self.shortRValues.append(self.shortR)
        self.phaseSwingRValues.append(self.phaseSwingR)
        self.fSwingRValues.append(self.fSwingR)
        self.leftKneePitchValues.append(self.leftKneePitch)
        self.rightKneePitchValues.append(self.rightKneePitch)
        self.leftAnklePitchValues.append(self.leftAnklePitch)
        self.rightAnklePitchValues.append(self.rightAnklePitch)
        self.leftHipPitchValues.append(self.leftHipPitch)
        self.rightHipPitchValues.append(self.rightHipPitch)
        self.leftHipRollValues.append(self.leftHipRoll)
        self.rightHipRollValues.append(self.rightHipRoll)
        self.leftAnkleRollValues.append(self.leftAnkleRoll)
        self.rightAnkleRollValues.append(self.rightAnkleRoll)
        self.hipYawPitchValues.append(self.hipYawPitch)
        self.leftHipPitchValues.append(self.leftHipPitch)
        self.rightHipPitchValues.append(self.rightHipPitch)
        
    def runCycle(self):
        self.reset()
        while self.cnt <= 1:
            self.update()
            self.saveValues()

    def update(self):
        self.updateDamp()
        self.updatePhaseTrunk()
        self.updatePhaseLegR()
        self.updatePhaseLegL()
        self.updateRHipRoll()
        self.updateLHipRoll()
        self.updatePhaseShortR()
        self.updatePhaseShortL()
        self.updateShortR()
        self.updateShortL()
        self.updatePhaseSwingR()
        self.updateFSwingR()
        self.updatePhaseSwingL()
        self.updateFSwingL()
        self.updateLeftKneePitch()
        self.updateRightKneePitch()
        self.updateLeftAnklePitch()
        self.updateRightAnklePitch()
        self.updateRightHipPitch()
        self.updateLeftHipPitch()
        self.updateLeftHipRoll()
        self.updateRightHipRoll()
        self.updateLeftAnkleRoll()
        self.updateRightAnkleRoll()
        self.updateHipYawPitch()
        self.updateLeftHipPitch()
        self.updateRightHipPitch()
        self.updateCounter()

    def printValues(self):
        config.printArray(self.phaseTrunkValues)
        config.printArray(self.phaseLegRValues)
        config.printArray(self.RHipRollValues) 
        config.printArray(self.phaseShortRValues)
        config.printArray(self.shortRValues)     
        config.printArray(self.phaseSwingRValues)
        config.printArray(self.fSwingRValues)
        
    def updateDamp(self):
        self.damp = (self.startLength - self.dampStop) / self.startLength
        self.damp = max(min(self.damp, 1), 0)
        self.dampStop = max(self.dampStop-1, 0)

    def updatePhaseTrunk(self):
        self.phaseTrunk = self.cnt * math.pi * 2 - math.pi

    def updatePhaseLegR(self):
        self.phaseLegR = self.phaseTrunk - math.pi / 2
        if self.phaseLegR < -math.pi:
            self.phaseLegR += math.pi * 2
        elif self.phaseLegR > math.pi:
            self.phaseLegR -= math.pi * 2
    
    def updatePhaseLegL(self):
        self.phaseLegL = self.phaseTrunk + math.pi / 2
        if self.phaseLegL < -math.pi:
            self.phaseLegL += math.pi * 2
        elif self.phaseLegL > math.pi:
            self.phaseLegL -= math.pi * 2
    
    def updateRHipRoll(self):
        self.RHipRoll = config.swing * math.sin(self.phaseLegR)

    def updateLHipRoll(self):
        self.LHipRoll = config.swing * math.sin(self.phaseLegL)

    def updatePhaseShortR(self):
        self.phaseShortR = config.vShort * (self.phaseLegL + math.pi / 2 + config.oShort)

    def updatePhaseShortL(self):
        self.phaseShortL = config.vShort * (self.phaseLegR + math.pi / 2 + config.oShort)
        
    def updateShortR(self):
        self.shortR = config.knee
        if self.phaseShortR > -math.pi and self.phaseShortR < math.pi:
            self.shortR += config.aShort * 0.5 * (math.cos(self.phaseShortR) + 1)

    def updateShortL(self):
        self.shortL = config.knee
        if self.phaseShortL > -math.pi and self.phaseShortL < math.pi:
            self.shortL += config.aShort * 0.5 * (math.cos(self.phaseShortL) + 1)

    def updatePhaseSwingR(self):
        self.phaseSwingR = config.vSwing * (self.phaseLegR + math.pi / 2 + config.oSwing)

    def updateFSwingR(self):
        self.fSwingR = -(2 / (2 * math.pi* config.vSwing - math.pi) * 
                (self.phaseSwingR + math.pi/ 2) + 1);
        if -math.pi/ 2 <= self.phaseSwingR and self.phaseSwingR < math.pi/ 2: 
            self.fSwingR = math.sin(self.phaseSwingR);
        elif math.pi/ 2 <= self.phaseSwingR:
            self.fSwingR = -(2 / (2 * math.pi* config.vSwing - math.pi) \
                    * (self.phaseSwingR - math.pi/ 2) - 1);

    def updatePhaseSwingL(self):
        self.phaseSwingL = config.vSwing * (self.phaseLegL + math.pi / 2 + config.oSwing)

    def updateFSwingL(self):
        self.fSwingL = -(2 / (2 * math.pi* config.vSwing - math.pi) * 
                (self.phaseSwingL + math.pi/ 2) + 1);
        if -math.pi/ 2 <= self.phaseSwingL and self.phaseSwingL < math.pi/ 2: 
            self.fSwingL = math.sin(self.phaseSwingL);
        elif math.pi/ 2 <= self.phaseSwingL:
            self.fSwingL = -(2 / (2 * math.pi* config.vSwing - math.pi) \
                    * (self.phaseSwingL - math.pi/ 2) - 1);

    def updateCounter(self):
        self.cnt += 1.0 / config.freq
        #print(self.cnt)
        self.it += 1

    def updateLeftKneePitch(self):
        self.leftKneePitch = self.shortR + 0.2 + self.balanceGyro1 * self.bodyPitch * self.damp;

    def updateRightKneePitch(self):
        self.rightKneePitch = self.shortL + 0.2 + self.balanceGyro1 * self.bodyPitch * self.damp;

    def updateLeftAnklePitch(self):
        self.leftAnklePitch = -self.shortR * 0.5 + - 0.18 -self.walkVecY * self.fSwingL + self.stabPitch

    def updateRightAnklePitch(self):
        self.rightAnklePitch = -self.shortL * 0.5 + -0.18 - self.walkVecY * self.fSwingR + self.stabPitch

    def updateLeftHipPitch(self):
        self.leftHipPitch = -self.shortR * 0.5 + self.balanceGyro2 *\
                self.bodyPitch * self.damp + self.walkVecY * self.fSwingR;

    def updateRightHipPitch(self):
        self.rightHipPitch = -self.shortL * 0.5 + self.balanceGyro2 *\
                self.bodyPitch * self.damp + self.walkVecY * self.fSwingL;

    def updateLeftHipRoll(self):
        self.leftHipRoll = self.RHipRoll + 0.02 + self.balanceGyro3 * self.bodyRoll * self.damp +\
                self.walkVecX * self.fSwingR + abs(self.walkVecX * 0.5);

    def updateRightHipRoll(self):
        self.rightHipRoll = -self.LHipRoll - 0.02 + self.balanceGyro3 * self.bodyRoll * self.damp -\
                self.walkVecX * self.fSwingR - abs(self.walkVecX * 0.5);

    def updateRightAnkleRoll(self):
        self.rightAnkleRoll = -self.RHipRoll - 0.02 + self.balanceGyro4 * self.bodyRoll * self.damp -\
                self.walkVecX * self.fSwingR - abs(self.walkVecX * 0.5); #+ self.stabRoll;

    def updateLeftAnkleRoll(self):
        self.leftAnkleRoll = self.LHipRoll + 0.02 + self.balanceGyro4 * self.bodyRoll * self.damp +\
                self.walkVecX * self.fSwingR + abs(self.walkVecX * 0.5)  # + self.stabRoll;

    def updateHipYawPitch(self):
        self.hipYawPitch =  self.walkVecA * (self.fSwingL - self.fSwingR);
    
    def updateLeftHipPitch(self):
        self.leftHipPitch -= 0.75 * self.walkVecA * (self.fSwingL - self.fSwingR);

    def updateRightHipPitch(self):
        self.rightHipPitch -= 0.75 * self.walkVecA * (self.fSwingL - self.fSwingR);
    

if __name__ == '__main__':
    w = WalkSim()
    w.runCycle()
    w.printValues()
