"""
INIT_VAR(balanceGyro1, -0.5f, "")
INIT_VAR(balanceGyro2, 0.45f, "")
INIT_VAR(balanceGyro3, 0.8f, "")
INIT_VAR(balanceGyro4, -0.21, "")

INIT_VAR(swing, 0.0457f, "")
INIT_VAR(vSwing, 0.738f, "")
INIT_VAR(oSwing, -0.82f, "")

INIT_VAR(knee, 0.22f, "")

INIT_VAR(vShort, 2.1f, "")
INIT_VAR(aShort, 0.5f, "")
INIT_VAR(oShort, -1.2f, "")

INIT_VAR(gyrometerX, 2431, "")
INIT_VAR(gyrometerY, 2431, "")

INIT_VAR_RW(scale_odo_x, 0.018f, "scaling factor for odo calculation X")
INIT_VAR_RW(scale_odo_y, 0.012f, "scaling factor for odo calculation Y")
INIT_VAR_RW(scale_odo_a, 0.055f, "scaling factor for odo calculation alpha")

INIT_VAR(max_acceleration, 0.0125f, "max acceleration steps ~[0.02...0.04]")
INIT_VAR(base_speed_limit, 0.11f, "base speed limiting var ~0.14f")

INIT_VAR(min_freq, 48, "minimal frequency (if freq-scaling is used)")
INIT_VAR(max_freq, 48, "maximal frequency (if freq-scaling is used)")

INIT_VAR(damping_duration, 40, "how many iters the start-damping is applied")
"""

balanceGyro1 = -0.5;
balanceGyro2 = -0.45;
balanceGyro3 = 0.8;
balanceGyro4 = -0.21;

gyroX = 0;
gyroY = 0;

swing = 0.0457;
vSwing = 0.738;
oSwing = -0.82;

knee = 0.22;

vShort = 2.1;
aShort = 0.5;
oShort = -1.2;

gyrometerX = 2431;
gyrometerY = 2431;

freq = 48;



def printArray(arrayListe):
    output = ""
    output = output + "["
    for i in arrayListe:
        output = output + str(i)
        if(i != arrayListe[(len(arrayListe)-1)]):
            output = output + ", "
    output = output + "]"
    output = output + "\n"
    print(output)


